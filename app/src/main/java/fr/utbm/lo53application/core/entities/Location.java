package fr.utbm.lo53application.core.entities;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author      Julien PETIT
 * @author      Ruby Hossain Saib
 * @version     %I%, %G%
 * @since       1.0
 */
public class Location {

    private static final String JSON_FIELD_COORDINATE_X = "coordinateX";
    private static final String JSON_FIELD_COORDINATE_Y = "coordinateY";

    private int coordinateX;
    private int coordinateY;

    /**
     * Create a Location object from coordinates
     * @param coordinateX coordinate X of the location instanciated
     * @param coordinateY coordinate X of the location instanciated
     */
    public Location(int coordinateX, int coordinateY) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
    }

    /**
     * Create Location object from JSONObject
     * @param object JSON object containing valid location informations
     * @throws JSONException
     */
    public Location(JSONObject object) throws JSONException {
        this.coordinateX = object.getInt(JSON_FIELD_COORDINATE_X);
        this.coordinateY = object.getInt(JSON_FIELD_COORDINATE_Y);
    }

    /**
     *
     * @return the coordinate X of the location
     */
    public int getCoordinateX() {
        return coordinateX;
    }

    /**
     * Set the coordinate X of the location
     * @param coordinateX the new coordinate X
     */
    public void setCoordinateX(int coordinateX) {
        this.coordinateX = coordinateX;
    }

    /**
     *
     * @return the coordinate Y of the location
     */
    public int getCoordinateY() {
        return coordinateY;
    }

    /**
     * Set the coordinate Y of the location
     * @param coordinateY the new coordinate Y
     */
    public void setCoordinateY(int coordinateY) {
        this.coordinateY = coordinateY;
    }

    @Override
    public String toString() {
        return "Location{" +
                "coordinateY=" + coordinateY +
                ", coordinateX=" + coordinateX +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        if (coordinateX != location.coordinateX) return false;
        return coordinateY == location.coordinateY;

    }

    @Override
    public int hashCode() {
        int result = coordinateX;
        result = 31 * result + coordinateY;
        return result;
    }
}
