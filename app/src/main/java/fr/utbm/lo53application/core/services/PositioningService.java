package fr.utbm.lo53application.core.services;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import fr.utbm.lo53application.core.entities.Map;
import fr.utbm.lo53application.core.listeners.DoCalibrateLocationListener;
import fr.utbm.lo53application.core.listeners.GetDeviceLocationListener;
import fr.utbm.lo53application.core.entities.Location;
import fr.utbm.lo53application.core.listeners.GetMapListListener;

/**
 * @author      Julien PETIT
 * @version     %I%, %G%
 * @since       1.0
 */
public class PositioningService {

    public final static String TAG                      = "PositioningService";
    public final static String SERVER_URL               =  "http://api.julienpetit.fr"; //"http://192.168.1.112:8888" ;
//    public final static String REQUEST_LOCATION_URL     = SERVER_URL + "/locate?map_id=%d&mac_address=%s";
//    public final static String REQUEST_CALIBRATION_URL  = SERVER_URL + "/calibrate?map_id=%d&mac_address=%s&x=%d&y=%d";
//    public final static String REQUEST_MAP_LIST_URL     = SERVER_URL + "/maplist";
    public final static String REQUEST_LOCATION_URL     = SERVER_URL + "/location.json?map_id=%d&mac_address=%s";
    public final static String REQUEST_CALIBRATION_URL  = SERVER_URL + "/calibration.json?map_id=%d&mac_address=%s&x=%d&y=%d";
    public final static String REQUEST_MAP_LIST_URL     = SERVER_URL + "/maps.json";

    /**
     * Send a request to the positioning server to get device location
     * @param listener
     */
    public JsonObjectRequest getDeviceLocation(final GetDeviceLocationListener listener, Map map, String macAddress) {

        Log.i(TAG, "URL " + String.format(REQUEST_LOCATION_URL, map.getIdentifier(), macAddress));

        return new JsonObjectRequest(
            Request.Method.GET,
            String.format(REQUEST_LOCATION_URL, map.getIdentifier(), macAddress),
            null,
            new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    // transform response in Location object and call
                    try {

                        Location location = new Location(response.getJSONObject("location"));
                        listener.onSuccessGetDeviceLocation(location);

                    } catch (JSONException e) {

                        Log.e(TAG, "Location request : Unable to create a new Location object from JSONResponse " + e.getMessage());
                        listener.onErrorGetDeviceLocation("Unable to create a new Location object from JSONResponse");

                    }

                }
            },
            new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    String message = null;

                    try {
                        if(error.networkResponse != null)
                            message = new String(error.networkResponse.data,"UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        message = "Unknown message";
                    }

                    // Send error message to the listener
                    listener.onErrorGetDeviceLocation(message);
                }
            }
        );

    }

    /**
     *
     * @param listener
     * @param location
     */
    public JsonObjectRequest doCalibrateLocation(final DoCalibrateLocationListener listener, final Location location, Map map, String macAddress) {

        Log.i(TAG, "URL " + String.format(REQUEST_CALIBRATION_URL, map.getIdentifier(), macAddress, location.getCoordinateX(), location.getCoordinateY()));

        return new JsonObjectRequest(
                Request.Method.GET,
                String.format(REQUEST_CALIBRATION_URL, map.getIdentifier(), macAddress, location.getCoordinateX(), location.getCoordinateY()),
                null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            Location locationReceived = new Location(response.getJSONObject("location"));

                            if(location.equals(locationReceived)) {
                                listener.onSuccessDoCalibrateListener(location);
                            } else {
                                throw new Exception("Unable to calibrate the location " + location.toString());
                            }

                        } catch (JSONException e) {

                            e.printStackTrace();

                            Log.e(TAG, "Calibration request : " + e.getMessage());
                            listener.onErrorDoCalibrateListener(location, "Unable to calibrate the location " + location.toString());

                        } catch (Exception e) {

                            e.printStackTrace();

                            Log.e(TAG, "Calibration request : " + e.getMessage());
                            listener.onErrorDoCalibrateListener(location, e.getMessage());

                        }

                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        String message = null;

                        try {
                            if(error.networkResponse != null)
                                message = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            message = "Unknown message";
                        }

                        // Send error message to the listener
                        listener.onErrorDoCalibrateListener(location, message);
                    }
                }
        );

    }

    /**
     *
     * @param listener
     */
    public JsonObjectRequest getMapList(final GetMapListListener listener) {

        return new JsonObjectRequest(
                Request.Method.GET,
                REQUEST_MAP_LIST_URL,
                null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        JSONArray jsonMaps  = null;
                        ArrayList<Map> maps = new ArrayList<>();

                        try {
                            jsonMaps = response.getJSONArray("maps");

                            for (int i = 0; i < jsonMaps.length(); i++) {

                                JSONObject map = jsonMaps.getJSONObject(i);
                                maps.add(new Map(map));

                            }

                            listener.onSuccessGetMapListListerner(maps);

                        } catch (JSONException e) {
                            Log.e(TAG, "Get map list : " + e.getMessage());
                            listener.onErrorGetMapListListerner(e.getMessage());
                        }

                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        error.printStackTrace();

                        String message = null;

                        try {
                            if(error.networkResponse != null)
                                message = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            message = "Unknown message";
                        } catch (Exception e) {
                            message = e.getMessage();
                        }

                        // Send error message to the listener
                        listener.onErrorGetMapListListerner(message);
                    }
                }
        );

    }

}
