package fr.utbm.lo53application.core.entities;

import android.media.Image;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Class Map
 */
public class Map implements Serializable {

    private static final String JSON_FIELD_IDENTIFIER  = "id";
    private static final String JSON_FIELD_TITLE       = "name";
    private static final String JSON_FIELD_IMAGE       = "image";

    private long identifier;
    private Image image;
    private String imageUrl;
    private String title;
    private int widthSize = 8;
    private int heightSize = 5;

    /**
     *
     * @param identifier id of the map
     * @param image image of the map
     * @param title string title of the map
     */
    public Map(long identifier, Image image, String title) {
        this.identifier = identifier;
        this.image = image;
        this.imageUrl = null;
        this.title = title;
    }

    /**
     *
     * @param object JSON object containing valid map informations
     * @throws JSONException
     */
    public Map(JSONObject object) throws JSONException{
        this.identifier = object.getInt(JSON_FIELD_IDENTIFIER);
        this.image      = null;
        this.imageUrl   = object.getString(JSON_FIELD_IMAGE);
        this.title      = object.getString(JSON_FIELD_TITLE);
    }

    public long getIdentifier() {
        return identifier;
    }

    public void setIdentifier(long identifier) {
        this.identifier = identifier;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getWidthSize() {
        return widthSize;
    }

    public void setWidthSize(int widthSize) {
        this.widthSize = widthSize;
    }

    public int getHeightSize() {
        return heightSize;
    }

    public void setHeightSize(int heightSize) {
        this.heightSize = heightSize;
    }

    @Override
    public String toString() {
        return "Map{" +
                ", identifier=" + identifier +
                ", image=" + image +
                ", title='" + title + '\'' +
                '}';
    }
}
