package fr.utbm.lo53application.core.listeners;

import fr.utbm.lo53application.core.entities.Location;

/**
 * @author      Julien PETIT
 * @version     %I%, %G%
 * @since       1.0
 */
public interface GetDeviceLocationListener {

    void onSuccessGetDeviceLocation(Location location);
    void onErrorGetDeviceLocation(String error);

}
