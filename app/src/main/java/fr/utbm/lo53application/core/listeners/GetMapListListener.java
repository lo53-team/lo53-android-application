package fr.utbm.lo53application.core.listeners;

import java.util.ArrayList;

import fr.utbm.lo53application.core.entities.Location;
import fr.utbm.lo53application.core.entities.Map;

/**
 * @author      Julien PETIT
 * @version     %I%, %G%
 * @since       1.0
 */
public interface GetMapListListener {

    void onSuccessGetMapListListerner(ArrayList<Map> maps);
    void onErrorGetMapListListerner(String error);

}
