package fr.utbm.lo53application.utils;

/**
 * @author      Julien PETIT
 * @version     %I%, %G%
 * @since       1.0
 */
public class Log {

    public static void info(String classname, String message) {
        System.out.println(classname + " : " + message);
    }

    public static void error(String classname, String message) {
        System.err.println(classname + " : " + message);
    }



}
