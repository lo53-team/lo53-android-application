package fr.utbm.lo53application.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.media.Image;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import fr.utbm.lo53application.R;
import fr.utbm.lo53application.core.entities.Location;
import fr.utbm.lo53application.core.entities.Map;

/**
 * @author      Julien PETIT
 * @version     %I%, %G%
 * @since       1.0
 */
public class MapTilesAdapter implements View.OnClickListener {

    private Activity activity;
    private LinearLayout mapLocationsContainer;
    private Map map;

    private ArrayList<ImageButton> listTiles;

    private ArrayList<Location> listCalibratedLocations;

    private TextView tvSelectedLocation;

    private ImageView ivCurrentLocation;
    private ImageView ivCurrentSelection;

    public MapTilesAdapter(Activity activity, Map map, LinearLayout mapLocationsContainer, TextView tvSelectedLocation) {
        this.activity                = activity;
        this.mapLocationsContainer   = mapLocationsContainer;
        this.map                     = map;
        this.listTiles               = new ArrayList<>();
        this.listCalibratedLocations = new ArrayList<>();
        this.ivCurrentLocation       = null;
        this.ivCurrentSelection      = null;
        this.tvSelectedLocation      = tvSelectedLocation;

        // Example
        this.listCalibratedLocations.add(new Location(3, 5));
        this.listCalibratedLocations.add(new Location(3, 3));

        this.setTvLocation(null);
    }

    public void makeTiles() {

        listTiles = new ArrayList<>();
        mapLocationsContainer.removeAllViews();

        for(int i = 0; i < map.getHeightSize(); i++){

            LinearLayout line = new LinearLayout(activity);
            line.setOrientation(LinearLayout.HORIZONTAL);
            line.setWeightSum(map.getWidthSize());
            LinearLayout.LayoutParams LLParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 80);
            LLParams.weight = 1;
            line.setLayoutParams(LLParams);

            for (int j = 0; j < map.getWidthSize(); j++){

                ImageButton tile = new ImageButton(activity);
                tile.setImageResource(R.drawable.no_pin_bullet_gray);
                tile.setTag(new Location(j, i));
                tile.setBackgroundColor(Color.TRANSPARENT);

                LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                p.weight = 1;

                tile.setLayoutParams(p);
                tile.setBackground(activity.getDrawable(R.drawable.border_image));

                tile.setOnClickListener(this);

                listTiles.add(tile);
                line.addView(tile);
            }

            mapLocationsContainer.addView(line);
        }

    }

    public void setLocation(Location location) {

        // Resetting the current selection if it is the same location because the tile image will be overrided
        if(this.ivCurrentSelection != null && location.equals(this.ivCurrentSelection.getTag())) {
            this.ivCurrentSelection = null;
            setTvLocation(null);
        }

        // Removing the current location to make the old image tile disappearing
        if(this.ivCurrentLocation != null) {

            Location oldLocation = (Location) this.ivCurrentLocation.getTag();

            if (listCalibratedLocations.contains(oldLocation)) {
                this.ivCurrentLocation.setImageResource(R.drawable.no_pin_bullet_green);
            } else {
                this.ivCurrentLocation.setImageResource(R.drawable.no_pin_bullet_gray);
            }
        }

        // Trying to get the current tile and set his image
        for (ImageButton tile : listTiles) {
            if(location.equals(tile.getTag())) {

                this.ivCurrentLocation = tile;

                if (listCalibratedLocations.contains(location)) {

                    tile.setImageResource(R.drawable.pin_orange_bullet_green);
                } else {
                    tile.setImageResource(R.drawable.pin_orange_bullet_gray);
                }
            }
        }

    }

    /**
     *
     * @param location
     */
    public void setLocationCalibrated(Location location) {

        for (ImageButton tile : listTiles) {
            if(location.equals(tile.getTag())) {
                tile.setImageResource(R.drawable.no_pin_bullet_green);
                this.ivCurrentSelection = null;
                setTvLocation(null);
            }
        }

        this.listCalibratedLocations.add(location);
    }

    /**
     *
     * @param location
     */
    public void setLocationSelected(Location location, ImageButton tile) {

        // Removing the current selection
        removeCurrentLocation();

        this.ivCurrentSelection = tile;

        if (listCalibratedLocations.contains(location)) {
            tile.setImageResource(R.drawable.pin_green_bullet_green);
        } else {
            tile.setImageResource(R.drawable.pin_green_bullet_gray);
        }

        this.setTvLocation(location);
    }

    /**
     *
     */
    public void removeCurrentLocation() {

        if (this.ivCurrentSelection != null) {

            Location oldLocation = (Location) this.ivCurrentSelection.getTag();

            if (listCalibratedLocations.contains(oldLocation)) {
                this.ivCurrentSelection.setImageResource(R.drawable.no_pin_bullet_green);
            } else {
                this.ivCurrentSelection.setImageResource(R.drawable.no_pin_bullet_gray);
            }
        }

        this.ivCurrentSelection = null;
    }

    @Override
    public void onClick(View v) {
        if(v instanceof ImageButton && v.getTag() instanceof Location) {
            setLocationSelected((Location) v.getTag(), (ImageButton) v);
        }
    }

    /**
     *
     * @return
     */
    public Location getCurrentLocationSelected() {
        Location location = null;

        if(ivCurrentSelection != null && ivCurrentSelection.getTag() instanceof Location) {
            location = (Location) ivCurrentSelection.getTag();
        }

        return location;
    }

    private void setTvLocation(Location location) {
        if(location != null) {
            this.tvSelectedLocation.setText("Selected reference point : \nx : " + location.getCoordinateX() + "\ny : " + location.getCoordinateY());
        } else {
            this.tvSelectedLocation.setText("No reference point selected");
        }
    }
}
