package fr.utbm.lo53application.ui.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.koushikdutta.urlimageviewhelper.UrlImageViewCallback;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import fr.utbm.lo53application.R;
import fr.utbm.lo53application.core.entities.Map;
import fr.utbm.lo53application.core.listeners.DoCalibrateLocationListener;
import fr.utbm.lo53application.core.listeners.GetDeviceLocationListener;
import fr.utbm.lo53application.core.entities.Location;
import fr.utbm.lo53application.core.services.PositioningService;
import fr.utbm.lo53application.ui.adapters.MapTilesAdapter;

/**
 * @author      Julien PETIT
 * @author      Ruby Hossain Saib
 * @version     %I%, %G%
 * @since       1.0
 */
public class MapActivity extends FragmentActivity implements GetDeviceLocationListener, DoCalibrateLocationListener, View.OnClickListener {

    private static String TAG = "MapActivity";


    /**
     * Service used to generate requests
     */
    private PositioningService positioningService;

    /**
     * Map entity fetch from the map list
     */
    private Map map;

    /**
     * Toolbar instance fetched from the XML view
     */
    private Toolbar toolbar;

    /**
     * ImageView instance fetched from the XML view
     */
    private ImageView mapImageView;

    /**
     * Adapter to manage the map tiles squares
     */
    private MapTilesAdapter mapTilesAdapter;

    /**
     * Reference of the macAddresse of the device
     */
    private String macAddress;

    /**
     * Request queue to send requests
     */
    private RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        // Fetching the map instance from previous activity
        this.map = (Map) getIntent().getSerializableExtra("map");

        // Setting title of the view with the room's title
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.toolbar.setTitle(map.getTitle() + " room");

        // Fetching buttons views
        FloatingActionButton buttonCalibration = (FloatingActionButton) findViewById(R.id.fabCalibration);
        FloatingActionButton buttonLocation    = (FloatingActionButton) findViewById(R.id.fabLocation);

        // Setting the listener of buttons locate and calibrate
        buttonCalibration.setOnClickListener(this);
        buttonLocation.setOnClickListener(this);

        // Initialize positioning service to make requests
        this.positioningService = new PositioningService();

        // Initialize new Queue to execute requests
        this.queue              = Volley.newRequestQueue(this);

        // Fetching device mac address
        WifiManager manager     = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiInfo info           = manager.getConnectionInfo();
        this.macAddress         = info.getMacAddress();

        mapImageView            = (ImageView) findViewById(R.id.mapImageView);

        mapTilesAdapter         = new MapTilesAdapter(
                this,
                map,
                (LinearLayout) findViewById(R.id.mapLocationsContainer),
                (TextView) findViewById(R.id.tvLocation)
        );

        // Fetching the map image and build pin buttons
//        UrlImageViewHelper.setUrlDrawable(mapImageView, map.getImageUrl(), new UrlImageViewCallback() {
//            @Override
//            public void onLoaded(ImageView imageView, Bitmap loadedBitmap, String url, boolean loadedFromCache) {
//                mapTilesAdapter.makeTiles();
//            }
//        });

        this.mapImageView.setImageResource(R.drawable.h10landscape);
        mapTilesAdapter.makeTiles();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * This method is called when a location is done
     * @param location
     */
    @Override
    public void onSuccessGetDeviceLocation(Location location) {
        // TODO : display location on map
        mapTilesAdapter.setLocation(location);

        Toast.makeText(this, "Getting location success ! " + location.toString(), Toast.LENGTH_LONG).show();
    }

    /**
     * This method is called when a location is in error
     * @param error
     */
    @Override
    public void onErrorGetDeviceLocation(String error) {

        Log.i(TAG, "onErrorGetDeviceLocation error : " + error);

        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    /**
     * This method is called when a calibration is done with sucess
     * @param location
     */
    @Override
    public void onSuccessDoCalibrateListener(Location location) {
        // @TODO : Mark the location point as calibrated
        mapTilesAdapter.setLocationCalibrated(location);

        Toast.makeText(this, "Calibrating location success ! " + location.toString(), Toast.LENGTH_LONG).show();

    }

    /**
     * This method is called when a calibration failed
     * @param location
     * @param error
     */
    @Override
    public void onErrorDoCalibrateListener(Location location, String error) {

        Log.i(TAG, "onErrorDoCalibrateListener error : " + error + ", location : " + location.toString());

        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();

    }

    /**
     * Implementation of the method onClick fired when user touch a fab button
     * Calibration : send a calibration request
     * Location : send a location request
     * @param v
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.fabCalibration:
                Location locationSelected = mapTilesAdapter.getCurrentLocationSelected();

                if(locationSelected != null) {
                    queue.add(positioningService.doCalibrateLocation(MapActivity.this, locationSelected, map, macAddress));
                } else {
                    Toast.makeText(this, "You have to select a reference point before sending a calibration request", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.fabLocation:
                queue.add(positioningService.getDeviceLocation(this, map, macAddress));
                break;
        }
    }




}
