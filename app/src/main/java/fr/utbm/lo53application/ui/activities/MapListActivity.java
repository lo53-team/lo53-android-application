package fr.utbm.lo53application.ui.activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;

import fr.utbm.lo53application.R;
import fr.utbm.lo53application.core.entities.Map;
import fr.utbm.lo53application.core.listeners.GetMapListListener;
import fr.utbm.lo53application.core.services.PositioningService;
import fr.utbm.lo53application.ui.adapters.MapListAdapter;

/**
 * @author      Julien PETIT
 * @author      Ruby Hossain Saib
 * @version     %I%, %G%
 * @since       1.0
 */
public class MapListActivity extends AppCompatActivity implements GetMapListListener {

    private static String TAG = "MapListActivity";

    /**
     * map ListView referenced by XML
     */
    private ListView listViewMapList;

    /**
     * SwipeRefreshLayout referenced by XML
     */
    private SwipeRefreshLayout swipeContainer;

    /**
     * Service used to generate requests
     */
    private PositioningService positioningService;

    /**
     * Map list adapter, layer between view and models
     */
    private MapListAdapter mapListAdapter;

    /**
     * Request queue used to send requests generated from the positioning service
     */
    private RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_list);

        // Initialize view
        this.listViewMapList    = (ListView) findViewById(R.id.lvMapList);
        this.swipeContainer     = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);

        // Initialize positioning service
        this.positioningService = new PositioningService();

        // Initialize new Queue
        this.queue = Volley.newRequestQueue(this);

        // Initialisze the map view list adapter
        this.mapListAdapter = new MapListAdapter(this, new ArrayList<Map>());

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // Start fetchin map list
                reloadData();
            }
        });

        reloadData();

        this.listViewMapList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Map map = (Map) parent.getAdapter().getItem(position);

                Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                intent.putExtra("map", map);
                startActivity(intent);

            }
        });

    }

    /**
     * This method request the map list from the server
     */
    private void reloadData() {
        // Start fetchin map list
        queue.add(positioningService.getMapList(MapListActivity.this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map_list, menu);
        return true;
    }

    /**
     * Callback called when the map list is successfully loaded
     * This method refresh the map list with the informations fetched by the positioning server
     *
     * @param maps
     */
    @Override
    public void onSuccessGetMapListListerner(ArrayList<Map> maps) {

        swipeContainer.setRefreshing(false);

        Log.i(TAG, "onSuccessGetMapListListerner maps count : " + maps.size());

        mapListAdapter.clear();
        mapListAdapter.addAll(maps);

        this.listViewMapList.setAdapter(mapListAdapter);
    }

    /**
     * Callback called when an error occured when fetchin the map list
     * This method display a Toast with the aim of the error and stop the loading view
     * @param error
     */
    @Override
    public void onErrorGetMapListListerner(String error) {

        swipeContainer.setRefreshing(false);

        Log.i(TAG, "onErrorGetMapListListerner error : " + error);

        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }


}
