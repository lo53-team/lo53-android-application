package fr.utbm.lo53application.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import fr.utbm.lo53application.R;
import fr.utbm.lo53application.core.entities.Map;

/**
 * @author      Julien PETIT
 * @author      Ruby Hossain Saib
 * @version     %I%, %G%
 * @since       1.0
 */
public class MapListAdapter extends ArrayAdapter<Map> {

    public MapListAdapter(Context context, ArrayList<Map> maps) {
        super(context, 0, maps);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Map map = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.map_list_item, parent, false);
        }
        // Lookup view for data population
        TextView tvTitle = (TextView) convertView.findViewById(R.id.tvMapTitle);
        TextView tvSubTitle = (TextView) convertView.findViewById(R.id.tvMapSubTitle);
        TextView tvId = (TextView) convertView.findViewById(R.id.tvMapId);
        // Populate the data into the template view using the data object
        tvTitle.setText(map.getTitle());
        tvSubTitle.setText("Map of the " + map.getTitle() + " room");
        tvId.setText("(" + map.getIdentifier() + ")");
        // Return the completed view to render on screen
        return convertView;
    }

}
