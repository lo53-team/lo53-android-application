\mainpage Getting started

\section install_sec Installation

\subsection prerequisites Prerequisites

* Android SDK
* Android device >= 5.0

\subsection step1 Step1 : Clone the repository

Clone the repository in Android Studio by using the following command :

> `git clone git@bitbucket.org:lo53-team/lo53-android-application.git`

\subsection step2 Step2 : Configure server IP

Open the class located at `fr.utbm.lo53application.core.services.PositioningService` and edit the field SERVER_URL:

``` java
public class PositioningService {

    public final static String SERVER_URL = "http://192.168.1.112:8888" ;

}
```

\subsection step3 Step3 : Build & launch the application

Build and lauche the app from Android Studio

